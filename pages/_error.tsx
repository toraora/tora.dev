import React from 'react';
const Error = () => (
  <>
    <p style={{ fontSize: '1.3rem' }}>Oops! There's nothing here.</p>
  </>
);

export default Error;
