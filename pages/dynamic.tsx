import React from 'react';

class Dynamic extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  handleButtonClick = () => {
    this.setState((state: any) => ({
      count: state.count + 1,
    }));
  };

  render() {
    return (
      <p>
        Count is: {this.state.count}
        <br />
        <button onClick={this.handleButtonClick}>Add 1</button>
      </p>
    );
  }
}

export default Dynamic;
