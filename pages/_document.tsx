import Document, { Html, Main, DocumentContext } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="en">
        <head>
          {this.props.head}
          {this.props.styles}
        </head>
        <body>
          <Main />
        </body>
      </Html>
    );
  }
}

var ExportedDocument: any = MyDocument;

if (process.env.NODE_ENV === 'development') {
  ExportedDocument = Document;
}

export default ExportedDocument;
