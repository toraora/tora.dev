import React from 'react';
import App, { AppContext } from 'next/app';
import Head from 'next/head';

import GlobalStylesheet from '../components/global-stylesheet';
import Layout from '../components/layout';
import Favicon from '../components/favicon';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }: AppContext) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <>
        <Head>
          <title>Nathan Wong</title>
          <meta
            name="description"
            content="Personal website of Nathan, professional software developer"
            key="description"
          />
          <Favicon />
        </Head>
        <GlobalStylesheet />

        {pageProps.hideLayout ? (
          <Component {...pageProps} />
        ) : (
          <Layout>
            <Component {...pageProps} />
          </Layout>
        )}
      </>
    );
  }
}

export default MyApp;
