const withMDX = require('@next/mdx')({ extension: /\.mdx?$/ });

const config = {
  pageExtensions: ['tsx', 'mdx'],
  //  target: 'serverless',
  target: 'server',
};

module.exports = withMDX(config);
