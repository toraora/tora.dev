# deploying serverless instead of static

Change `builds` in `now.json` to the following:

`"builds": [{ "src": "next.config.js", "use": "@now/next" }],`
