const GlobalStylesheet = () => (
  <style jsx global>{`
    body {
      font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI',
        'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
        'Helvetica Neue', sans-serif;
      line-height: 1.4;

      max-width: 800px;
      margin: 20px auto;
      padding: 0 20px;
      overflow-y: scroll;

      text-rendering: optimizeLegibility;
    }

    .m-only {
      display: none;
    }
    @media screen and (max-width: 550px) {
      .d-only {
        display: none;
      }
      .m-only {
        display: initial;
      }
    }
  `}</style>
);

export default GlobalStylesheet;
