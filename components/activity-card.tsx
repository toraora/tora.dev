import React from 'react';

const ActivityCard = ({ children, title, subText, href, description }: any) => (
  <>
    <style jsx>
      {`
        .container {
          padding-top: 0.4rem;
          padding-left: 0.5rem;
          padding-right: 0.5rem;
          border: 1px solid black;
          border-radius: 5px;
          margin-bottom: 1rem;

          :global(p) {
            margin-bottom: 0.5rem;
          }

          :global(.header) {
            display: flex;
            justify-content: space-between;
          }

          :global(.title) {
            font-size: 1.2rem;
            font-weight: 700;
          }

          :global(.sub) {
            margin-left: 0.5rem;
            font-size: 0.9rem;
            font-weight: 400;
          }

          :global(.divider) {
            border: 0;
            border-top: 1px solid #888888;
          }
        }
      `}
    </style>
    <div className="container">
      <div className="header">
        {title && (
          <div className="title">
            {href ? <a href={href}>{title}</a> : title}
            {subText && <span className="sub">{subText}</span>}
          </div>
        )}
        {description && <div>{description}</div>}
      </div>
      <hr className="divider" />
      {children}
    </div>
  </>
);

export default ActivityCard;
