// favicon generated here:
// https://favicon.io/favicon-generator/?t=N&ff=Ubuntu&fs=100&fc=%23FFFFFF&b=circle&bc=%2308F

const Favicon = () => (
  <>
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/static/apple-touch-icon.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="32x32"
      href="/static/favicon-32x32.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="16x16"
      href="/static/favicon-16x16.png"
    />
    <link rel="manifest" href="/static/site.webmanifest" />
  </>
);

export default Favicon;
