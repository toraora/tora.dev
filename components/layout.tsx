import Link from 'next/link';

const HeaderBullet = () => (
  <>
    <style jsx>{`
      span {
        font-weight: 100;
        margin-right: 0.5rem;
      }
    `}</style>
    <span>></span>
  </>
);

const MobileNavLink = ({ href, children }: any) => (
  <>
    <style jsx>{`
      div {
        font-weight: 700;
      }
    `}</style>
    <div>
      <Link href={href}>
        <a>{children}</a>
      </Link>
    </div>
  </>
);

const Layout = ({ children }: any) => (
  <>
    <style jsx>
      {`
        .header {
          display: flex;
          justify-content: space-between;
          padding-left: 0px;
          padding-right: 0px;

          @media screen and (max-width: 550px) {
            flex-wrap: wrap;
          }
        }

        .header-item {
          font-weight: 700;
        }

        .name-wrapper {
          @media screen and (max-width: 550px) {
            flex-basis: 100%;
          }
        }

        .name {
          margin-top: 1rem;
          margin-bottom: 0;
          font-size: 2.5rem;

          @media screen and (max-width: 500px) {
            text-align: center;
            margin-top: 0;
            margin-bottom: 0.3rem;
          }
        }

        .m-nav {
          display: flex;
          flex: 1;
          justify-content: space-between;
          padding-left: 12px;
          padding-right: 12px;

          @media screen and (min-width: 551px) {
            display: none;
          }
        }

        hr {
          border: 1px solid black;
          margin-bottom: 2rem;

          @media screen and (max-width: 550px) {
            margin-bottom: 0;
          }
        }
      `}
    </style>
    <div className="header">
      <div className="name-wrapper">
        <h1 className="name">Nathan Wong</h1>
        <p className="header-item d-only">
          <HeaderBullet />
          Engineer at <a href="https://www.thumbtack.com">Thumbtack</a>
        </p>
      </div>

      <div className="d-only">
        <p className="header-item">
          <HeaderBullet />
          Learn more{' '}
          <Link href="/about">
            <a>about me</a>
          </Link>
        </p>
        <p className="header-item">
          <HeaderBullet />
          See some{' '}
          <Link href="/activities">
            <a>things I do</a>
          </Link>
        </p>
        <p className="header-item">
          <HeaderBullet />
          Read my{' '}
          <Link href="/blog">
            <a>blog</a>
          </Link>
        </p>
      </div>

      <div className="m-nav">
        <MobileNavLink href="/about">About Me</MobileNavLink>
        <MobileNavLink href="/activities">Things I Do</MobileNavLink>
        <MobileNavLink href="/blog">Blog</MobileNavLink>
      </div>
    </div>

    <hr />

    {children}
  </>
);

Layout.getInitialProps = async () => ({});

export default Layout;
